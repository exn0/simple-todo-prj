import Vue from 'vue'
import VModal from 'vue-js-modal'


import App from './App.vue'

Vue.use(VModal)


new Vue({
  el: '#app',
  render: h => h(App)
})
